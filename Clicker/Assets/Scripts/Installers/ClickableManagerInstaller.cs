﻿using UnityEngine;
using Zenject;
using Clicker.Core;

namespace Clicker.Installers
{
    public class ClickableManagerInstaller : MonoInstaller
    {
        [SerializeField]
        private ClickableManager clickableManager;

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<ClickableManager>().FromInstance(clickableManager).AsSingle().NonLazy();
        }
    }
}