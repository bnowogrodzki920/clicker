﻿using Zenject;
using UnityEngine;
using Clicker.Core;
using Clicker.Spawn;

namespace Clicker.Installers
{
    public class SpawnerInstaller : MonoInstaller
    {
        [SerializeField]
        private Transform clickableHolder;
        [SerializeField]
        private Clickable prefab;
        [SerializeField]
        private Spawner.Settings settings;

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<Spawner>().AsSingle().NonLazy();
            Container.BindInstance(settings).AsSingle();
            Container.BindMemoryPool<Clickable, ClickableMemoryPool>()
                .WithInitialSize(settings.initialClickableCount)
                .FromComponentInNewPrefab(prefab)
                .UnderTransform(clickableHolder);
        }
    }

}