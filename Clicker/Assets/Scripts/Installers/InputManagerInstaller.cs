﻿using Zenject;
using UnityEngine;
using UnityEngine.InputSystem;
using Clicker.Input;

namespace Clicker.Installers
{
    public class InputManagerInstaller : MonoInstaller
    {
        [SerializeField]
        private PlayerInput playerInput;

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<InputManager>().AsSingle().NonLazy();
            Container.BindInstance(playerInput).WhenInjectedInto<InputManager>();
        }
    }

}