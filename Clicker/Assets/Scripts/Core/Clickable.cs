﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Clicker.Core
{
    public class Clickable : MonoBehaviour, IPointerDownHandler
    {
        private IClickableManager clickableManager;
        private bool isLeft;

        public bool IsLeft => isLeft;


        [Zenject.Inject]
        private void Construct(IClickableManager clickableManager)
        {
            this.clickableManager = clickableManager;
        }

        public void Reinitialize(bool isLeft, Vector3 pos)
        {
            this.isLeft = isLeft;
            transform.position = pos;
        }

        public void UpdatePositionY(int value)
        {
            Vector3 newPos = transform.position;
            newPos.y -= value;
            transform.position = newPos;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            clickableManager.OnPressed(this);
        }
    }

}