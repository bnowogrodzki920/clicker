﻿using Clicker.Spawn;
using Clicker.Input;
using Clicker.UI;
using UnityEngine;
using System.Collections.Generic;

namespace Clicker.Core
{
    public class ClickableManager : MonoBehaviour, IClickableManager
    {
        [SerializeField]
        private int winningYPos = -4;

        private ISpawner spawner;
        private IInputManager inputManager;
        private IScoreView scoreView;
        private IPauseView pauseView;

        private List<Clickable> clickables = new List<Clickable>();

        private int tempScore = 0; // TODO move to score controller
        private int tempHighScore = 0;

        [Zenject.Inject]
        private void Construct(ISpawner spawner, IInputManager inputManager, IScoreView scoreView, IPauseView pauseView)
        {
            this.spawner = spawner;
            this.inputManager = inputManager;
            this.scoreView = scoreView;
            this.pauseView = pauseView;
        }

        private void Start()
        {
            inputManager.OnLeftPressed += OnLeftButtonPressed;
            inputManager.OnRightPressed += OnRightButtonPressed;

            SpawnClickables();

            // TODO move to score controller
            tempHighScore = PlayerPrefs.GetInt("highscore", 0);
        }

        private void SpawnClickables()
        {
            for (int i = 0; i < 12; i++) // temp hardcode
            {
                var clickable = spawner.Spawn();
                clickables.Add(clickable);
                clickable.UpdatePositionY(i);
            }
        }

        private Clickable SpawnNewClickable()
        {
            var clickable = spawner.Spawn();
            clickables.Add(clickable);
            return clickable;
        }

        private void DespawnClickable(Clickable clickable)
        {
            spawner.Despawn(clickable);
            clickables.Remove(clickable);
        }

        private void UpdateClickables()
        {
            foreach (var clickable in clickables)
            {
                clickable.UpdatePositionY(1);
            }
        }

        public void OnPressed(Clickable clickable)
        {
            //TODO mouse
        }

        private void OnLeftButtonPressed()
        {
            var clickable = clickables.Find(x => x.transform.position.y == winningYPos && x.IsLeft);
            HandleScore(clickable);
        }

        private void OnRightButtonPressed()
        {
            var clickable = clickables.Find(x => x.transform.position.y == winningYPos && !x.IsLeft);
            HandleScore(clickable);
        }

        private void HandleScore(Clickable clickable)
        {
            if (clickable != null)
            {
                IncreaseScore();
                UpdateClickables();
                DespawnClickable(clickable);
                SpawnNewClickable();
            }
            else if (clickable == null || clickable.transform.position.y < winningYPos)
            {
                pauseView.Show();
                inputManager.DisableInputs();
                Debug.Log("Lost");
            }
        }

        private void IncreaseScore()
        {
            tempScore++;

            if(tempScore > tempHighScore)
            {
                PlayerPrefs.SetInt("highscore", tempScore);
            }

            scoreView.UpdateText(tempScore.ToString());
        }

        private void OnDestroy()
        {
            inputManager.OnLeftPressed -= OnLeftButtonPressed;
            inputManager.OnRightPressed -= OnRightButtonPressed;
        }
    }
}