﻿namespace Clicker.Core
{
    public interface IClickableManager
    {
        void OnPressed(Clickable clickable);
    }
}