using UnityEngine;
using Clicker.Core;

namespace Clicker.Spawn
{
    public class Spawner : ISpawner
    {
        private readonly Settings settings;
        private readonly ClickableMemoryPool pool;

        public Spawner(Settings settings, ClickableMemoryPool pool)
        {
            this.settings = settings;
            this.pool = pool;
        }

        public Clickable Spawn()
        {
            bool isLeft = Random.value < 0.5;
            return pool.Spawn(isLeft, isLeft ? settings.leftSpawnPosition : settings.rightSpawnPosition);
        }

        public void Despawn(Clickable item)
        {
            pool.Despawn(item);
        }

        [System.Serializable]
        public class Settings
        {
            [field: SerializeField]
            public Vector2 leftSpawnPosition { get; private set; }
            [field: SerializeField]
            public Vector2 rightSpawnPosition { get; private set; }
            [field: SerializeField]
            public int initialClickableCount { get; private set; } = 32;
        }
    }

}