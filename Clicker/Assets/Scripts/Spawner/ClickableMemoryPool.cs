﻿using Zenject;
using UnityEngine;
using Clicker.Core;

namespace Clicker.Spawn
{
    public class ClickableMemoryPool : MonoMemoryPool<bool, Vector3, Clickable>
    {
        protected override void Reinitialize(bool isLeft, Vector3 pos, Clickable item)
        {
            item.Reinitialize(isLeft, pos);
        }
    }

}