﻿using Clicker.Core;

namespace Clicker.Spawn
{
    public interface ISpawner
    {
        void Despawn(Clickable item);
        Clickable Spawn();
    }
}