using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Clicker.UI
{
    public class PauseView : MonoBehaviour, IPauseView
    {
        [SerializeField]
        private TMP_Text highScoreText;

        public void Show()
        {
            gameObject.SetActive(true);
            highScoreText.text = string.Format("Highscore: {0}", PlayerPrefs.GetInt("highscore", 0).ToString());
        }

        public void Restart()
        {
            Debug.Log("Restart");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
