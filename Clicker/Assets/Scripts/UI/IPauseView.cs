﻿namespace Clicker.UI
{
    public interface IPauseView
    {
        void Show();
    }
}