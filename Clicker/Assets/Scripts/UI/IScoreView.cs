﻿namespace Clicker.UI
{
    public interface IScoreView
    {
        void UpdateText(string score);
    }
}