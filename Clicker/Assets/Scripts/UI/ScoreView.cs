using TMPro;
using UnityEngine;

namespace Clicker.UI
{
    public class ScoreView : MonoBehaviour, IScoreView
    {
        [SerializeField]
        private TMP_Text scoreText;

        public void UpdateText(string score)
        {
            scoreText.text = score;
        }
    }
}
