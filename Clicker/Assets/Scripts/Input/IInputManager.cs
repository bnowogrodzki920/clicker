﻿using System;

namespace Clicker.Input
{
    public interface IInputManager
    {
        event Action OnLeftPressed;
        event Action OnRightPressed;

        void DisableInputs();
    }
}