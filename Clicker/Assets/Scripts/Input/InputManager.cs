﻿using System;
using UnityEngine.InputSystem;

namespace Clicker.Input
{
    public class InputManager : IInputManager
    {
        private readonly PlayerInput playerInput;

        public event Action OnLeftPressed = delegate { };
        public event Action OnRightPressed = delegate { };

        public InputManager(PlayerInput playerInput)
        {
            this.playerInput = playerInput;
            // TODO refactor + move paths to settings
            playerInput.actions.actionMaps[0].actions[0].performed += OnLeftButtonPressed;
            playerInput.actions.actionMaps[0].actions[1].performed += OnRightButtonPressed;
        }

        private void OnLeftButtonPressed(InputAction.CallbackContext obj)
        {
            OnLeftPressed();
        }

        private void OnRightButtonPressed(InputAction.CallbackContext obj)
        {
            OnRightPressed();
        }

        public void DisableInputs()
        {
            playerInput.DeactivateInput();
        }
    }
}
